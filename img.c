#include "img.h"
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>


void bmp_copy_sz_and_hdr(int fdi, int fdo){

	char rdbuf[14];
	char bigrdbuf[1024];
	read(fdi, rdbuf, 14);

	lseek(fdi, 0, SEEK_SET);

	uint32_t sz = *((uint32_t*)(rdbuf+2));
	uint32_t header_sz = *((uint32_t*)(rdbuf+0xa));

	ftruncate(fdo, sz);

	for(int i = 0; i < header_sz/1024; i++){
		read(fdi, bigrdbuf, 1024);
		write(fdo, bigrdbuf, 1024);
	}
	if(header_sz%1024){
		read(fdi, bigrdbuf, header_sz%1024);
		write(fdo, bigrdbuf, header_sz%1024);
	}

	lseek(fdi, 0, SEEK_SET);
	lseek(fdo, 0, SEEK_SET);

}

struct image* bmp_to_img(int fd){
	struct image* img = (struct image*)malloc(sizeof(struct image));

	char rdbuf[0x1A];
	read(fd, rdbuf, 0x1A);

	img->width = *((uint32_t*)(rdbuf+0x12));
	img->height = *((uint32_t*)(rdbuf+0x16));

	img->pixeldata = (struct pixel*)malloc(img->width*img->height*3);

	uint32_t padding = 0;
	if((img->width * 3)%4){
		padding = 4 - (img->width * 3)%4;
	}

	uint32_t header_sz = *((uint32_t*)(rdbuf+0xa));
	
	lseek(fd, header_sz, SEEK_SET);

	struct pixel* cur = img->pixeldata;

	for(int i = 0; i < img->height; i++){
		read(fd, cur, img->width*3);
		cur += img->width;
		lseek(fd, padding, SEEK_CUR);
	}

	lseek(fd, 0, SEEK_SET);

	return img;

}	

void img_to_bmp(int fd, struct image* img){
	lseek(fd, 0, SEEK_SET);

	char rdbuf[14];
	read(fd, rdbuf, 14);

	uint32_t header_sz = *((uint32_t*)(rdbuf+0xa));

	lseek(fd, header_sz, SEEK_SET);

	uint32_t padding = 0;
	if((img->width * 3)%4){
		padding = 4 - (img->width * 3)%4;
	}

	char* cur = (char*)img->pixeldata;

	for(int i = 0; i < img->height; i++){
		write(fd, cur, img->width*3);
		lseek(fd, padding, SEEK_CUR);
		cur += img->width*3;
	}

}

void destroy_img(struct image* img){
	free(img->pixeldata);
	free(img);
}
