#include "img.h"
#include "sepia.h"
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/time.h>

extern void sepia_asm(void*, void*, uint32_t, uint32_t);

char key_c[3] = "-C\0";
char key_asm[5] = "-asm\0";

long long int current_timestamp(){
	struct timeval te; 
    gettimeofday(&te, NULL);
    uint64_t milliseconds = te.tv_sec*1000LL + te.tv_usec/1000;
    return milliseconds;
}

int main(int argc, char** argv){
	int func = 2;

	if(argc != 4){
		printf("Usage: ./main <input> <output> <key(-C/-asm)>\n");
		return 1;
	}
	if(!strcmp(argv[3], key_c)){
		func = 0;
	}else if(!strcmp(argv[3], key_asm)){
		func = 1;
	}else{
		printf("Wrong key!\n");
		return 4;
	}

	int fdi = open(argv[1], 0);
	if(fdi == -1){
		printf("Unable to open input file!\n");
		return 2;
	}
	int fdo = open(argv[2], 0x0042, 0x1b6);
	if(fdo == -1){
		printf("Unable to open output file!\n");
		return 3;
	}

	bmp_copy_sz_and_hdr(fdi, fdo);

	struct image* imgi = bmp_to_img(fdi);
	struct image* imgo = bmp_to_img(fdo);

	uint64_t time_mem = current_timestamp();

	if(!func){
		sepia(imgi, imgo);		
	}else{
		sepia_asm(imgo->pixeldata, imgi->pixeldata, imgi->width, imgi->height);	
	}
	printf("%lldms\n", current_timestamp()-time_mem); // approximates to ~9ms on a 1920x1080 image

	img_to_bmp(fdo, imgo);

	destroy_img(imgi);
	destroy_img(imgo);

	close(fdi);
	close(fdo);

	return 0;
}
