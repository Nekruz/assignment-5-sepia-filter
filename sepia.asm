global sepia_asm

section .data

align 16
red_coefficients dd 0.393, 0.349, 0.272, 0.0   
align 16
grn_coefficients dd 0.769, 0.686, 0.534, 0.0 
align 16
blu_coefficients dd 0.189, 0.168, 0.131, 0.0   
align 16
maxvalues dd 255, 255, 255, 0



section .text


sepia_asm:
	movups  xmm3, [red_coefficients]  
	movups  xmm4, [grn_coefficients]  
	movups xmm5, [blu_coefficients]  
	movups xmm6, [maxvalues]         
	mov r8d, edx 	; width counter
	.loop:
		pxor xmm0, xmm0
		pinsrb xmm0, [rsi], 0  
		pinsrb xmm0, [rsi], 4   
		pinsrb xmm0, [rsi], 8   
		cvtdq2ps xmm0, xmm0
		mulps xmm0, xmm3
		pxor xmm1, xmm1
		inc rsi
		pinsrb xmm1, [rsi], 0
		pinsrb xmm1, [rsi], 4
		pinsrb xmm1, [rsi], 8		
		cvtdq2ps xmm1, xmm1
		mulps xmm1, xmm4
		pxor xmm2, xmm2
		inc rsi
		pinsrb xmm2, [rsi], 0
		pinsrb xmm2, [rsi], 4
		pinsrb xmm2, [rsi], 8
		cvtdq2ps xmm2, xmm2
		mulps xmm2, xmm5
		inc rsi

		addps xmm0, xmm1
		addps xmm0, xmm2

		cvtps2dq xmm0, xmm0
		pminud xmm0, xmm6

		pextrb byte [rdi], xmm0, 8
		pextrb byte [rdi+1], xmm0, 4
		pextrb byte [rdi+2], xmm0, 0

		add rdi, 3
		dec r8d
	jnz .loop
		mov r8d, edx
		dec ecx
	jnz .loop
ret
