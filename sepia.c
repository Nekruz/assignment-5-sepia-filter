#include "sepia.h"
#include "img.h"
#include <stdint.h>
#include <stdio.h>

void sepia(struct image* imgi, struct image* imgo){
	float tmpR;
	float tmpG;
	float tmpB;

	for(int i = 0; i < imgi->width*imgi->height; i++){
		tmpB = (imgi->pixeldata+i)->R *0.393  + (imgi->pixeldata+i)->G *0.769 + (imgi->pixeldata+i)->B *0.189 ;
		tmpG = (imgi->pixeldata+i)->R *0.349  + (imgi->pixeldata+i)->G *0.686 + (imgi->pixeldata+i)->B *0.168 ;
		tmpR = (imgi->pixeldata+i)->R *0.272  + (imgi->pixeldata+i)->G *0.534 + (imgi->pixeldata+i)->B *0.131 ;

		if(tmpR > 255.0f){
			(imgo->pixeldata+i)->R = 255;
		}else{
			(imgo->pixeldata+i)->R = (uint8_t)tmpR;
		}
		if(tmpG > 255.0f){
			(imgo->pixeldata+i)->G = 255;
		}else{
			(imgo->pixeldata+i)->G = (uint8_t)tmpG;
		}
		if(tmpB > 255.0f){
			(imgo->pixeldata+i)->B = 255;
		}else{
			(imgo->pixeldata+i)->B = (uint8_t)tmpB;
		}

	}
}
