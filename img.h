#ifndef __IMG_H__
#define __IMG_H__

#include <stdint.h>

struct pixel{
	uint8_t R;
	uint8_t G;
	uint8_t B;	
};

struct image{
	uint32_t width;
	uint32_t height;

	struct pixel* pixeldata;

};


struct image* bmp_to_img(int fd);

void bmp_copy_sz_and_hdr(int fdi, int fdo);

void img_to_bmp(int fd, struct image* img);

void destroy_img(struct image* img);

#endif
